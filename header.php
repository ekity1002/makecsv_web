<!DOCTYPE html>
<html lang="ja">
      <head>
      <title>英単語検索ページ</title>
      <meta charset="UTF-8">
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="./jquery-json.js"></script>
      <script type="text/javascript" src="./search.js"></script>
      <link rel="stylesheet" type="text/css" href="style.css">
      </head>
