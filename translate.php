<?php
//error_reporting(0); //エラー出力オフ
ini_set("display_errors", 'On');
error_reporting(E_ALL);
header('content-type: application/json; charset=utf-8'); //json を帰す場合は指定する
require_once("./phpQuery-onefile.php"); // スクレイピング用ライブラリ
mb_language("ja"); //現在の言語設定
mb_internal_encoding('UTF-8'); //内部文字エンコーディング

$words=[];
if (isset($_POST["JSON"])) {
    $words=$_POST["JSON"]; // 配列取得
}

// 各単語の意味を検索して、その配列を返す
$translation = array();
$ch = curl_init();
for ($i = 0; $i < count($words); $i++){
    if (empty($words[$i])) {
        continue;
    }
    $word = str_replace(' ', '+', $words[$i]);
    $url = "https://ejje.weblio.jp/content/{$word}";
    //var_dump($url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);// SSLチェックしない。本当はよくない？
    $html = curl_exec($ch);
    //$html = file_get_contents($url);
    $doc_obj = phpQuery::newDocument(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8')); // オブジェクト化
    foreach ($doc_obj["meta"] as $meta){
        if (pq($meta)->attr("name") == "twitter:description") {
            // ltrim で余分な文字を削除
            /* これはだめ。rtrim が悪さしてるっぽい。
               $str = rtrim(ltrim(pq($meta)->attr("content"),"{$words[$i]}の意味や和訳。"),
                                     "1035万語収録！weblio辞書で英語学習");
            */
            //var_dump(ltrim(pq($meta)->attr("content"),"{$words[$i]}:"));
            $translation[$i] = ltrim(pq($meta)->attr("content"),"{$words[$i]}:");
        }
    }
}
curl_close($ch);
// 配列を返すときは json_encode
// 普通に返すのはダメ
$ret = json_encode($translation); //失敗した場合は false を返す.

if(json_last_error() == JSON_ERROR_NONE){
    echo $ret;
} else {
    http_response_code(500);
}
?>