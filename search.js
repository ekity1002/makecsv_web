function search(){
    var texts = document.getElementById("id1").value;
    var words = texts.split("\n");

    // URL用にエンコードする
    var encoded_words = [];
    for ( let i = 0; i < words.length; i++) {
        encoded_words.push(encodeURIComponent(words[i]));
    }

    // PHP でスクレイピング、単語の意味を取得
    $.ajax({
        type: "POST",
        url:  "./translate.php",
        data: {"JSON": words},
    }).done(function(data) {
        console.log("succes", data);
        setTranslationToForm(words, data);
        // 成功したら csv DLリンク作成
        document.getElementById("disp").innerHTML = '<a id="download" href="#" download="test.csv" onclick="exportTextFile()">CSVダウンロード</a>';
    }).fail(function(xhr, textStatus, errorThrown){
        console.log("error.  ",xhr.status, textStatus, errorThrown);
    });
}

// フォームをクリアする
function clearForm(){
    document.getElementById("id1").value = ""; //初期化
}


// フォーム内容をtextファイルに出力する
function exportTextFile(){
    // テキストファイル作成
    console.log("export");
    var bom = new Uint8Array([0xEF, 0xBB, 0xBF]);
    var content = document.getElementById("id1").value;
    console.log(content);
    var blob = new Blob([bom ,content], {"type" : "text/csv"});

    if (window.navigator.msSaveBlob) {
        window.navigator.msSaveBlob(blob, "test.csv");

        // msSaveOrOpenBlob の場合は保存せずに開ける
        window.navigator.msSaveOrOpenBlob(blob, "test.csv");
    } else {
        document.getElementById("download").href = window.URL.createObjectURL(blob);
        console.log(window.URL.createObjectURL(blob));
    }
}


// 翻訳結果をフォームにセットする。
function setTranslationToForm(words, translation){
    clearForm();
    //単語と意味をつなげてセットし直す
    for (let i = 0; i < words.length; i++) {
        if (words[i] == "" || !translation[i] || translation[i] == "1035万語収録！weblio辞書で英語学習"){
            continue;
        }
        translation[i] = translation[i].replace(/,/g, ".");// "/,/g" でカンマをドットに置き換える
        document.getElementById("id1").value += words[i] + ',' + translation[i] + "\n";
    }
}
